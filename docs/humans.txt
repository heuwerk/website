
                  .Y&@&#GJ^                       
                 7#@@@@@@@@G.                     
               ~G@@@@@@@@@#! !Y!.                 
             .5@@@@@@@@@&J :P@@@#Y^               
        :!?7~^!7YG#@@@@P: J&@@@@@@@Y              
      .P@@@@@&B5?~^^!J! !B@@@@@@@@Y. JP7:         
     ^B@@@@@@@@@@@&GY!:.P@@@@@@@G^ ~B@@@&P!       
    ~#@@@@@@@@@@@@@@@@@G~7@@@@#7 .5@@@@@@@@P      
   7&@@@@@@@@@@@@@@@@@@@&.?@@Y. 7#@@@@@@@@G^ !7:  
  J@@@@@@@@@@@@@@@@@@@@@B 7B~ :P@@@@@@@@#7 ^G@@&7 
 Y@@@@@@@@@#!7?Y5PGB##GJ^~!  J&@@@@@@@@Y..Y@@@@@@!
G@@@@@@@@@@&7    !?77?JY?: ~B@@@@@@@@G^ 7#@@@@@@@Y
P@@@@@@@@@@@@G!. .75B#G! .5@@@@@@@@#7 ^G@@@@@@@&J.
 5@@@@@@@@@@@@@#P~      7&@@@@@@@@Y. Y@@@@@@@@P~. 
  Y@@@@@@@@@@@@@@&^     G@@@@@@@G^ !#@@@@@@@#7^J: 
   J@@@@@@@@@@@@@@B.  7:.Y#&@@#7 :P@@@@@@@@Y.7G^  
    ?@@@@@@@@@@@@@@5  5&Y~:^~^. .#@@@@@@@G^:PG:   
     7@@@@@@@@@@@@@@7 Y@@@&BGGG^ Y@@@@@#7.?&5     
      !&@@@@@@@@@@@@&^J@@@@@@@@&J:^!??!~?#@?      
       ~&@@@@@@@@@@@@GY@@@@@@@@@@&GYJYG&@&~       
        ~#@@@@@@@@@@@@&@@@@@@@@@@@@@@@@@#^        
         ^#@@@@@@@@@@@@@@@@@@@@@@@@@@@@#^         
          :B@@@@@@@@@@@@@@@@@@@@@@@@@@@~          
           :G@@@@@@@@@@@@@@@@@@@@@@@@@Y           
            :PGGGGGGGGGGGGGGGGGGGGGGGG:           
            :!7777777777777777777777!~.           
           5@@@@@&J!!!!!!!!!!!!7P@@@@@#~          
          :@@@@@@?               #@@@@@Y          
          :@@@@@@7               B@@@@@Y          
          .#@@@@@P:.............!&@@@@@?          
           :7Y555PY????????????J55555J~           
           :J??????JJJJJJJJJJJJJ??????^           
           7@&&&&&&&&&&&&&&&&&&&&&&&&@7             


Gadgetbridge is licensed under the AGPLv3.
See https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/LICENSE for more details.

Contributors:
See https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/CONTRIBUTORS.rst

Artwork licenses:
https://codeberg.org/Freeyourgadget/Gadgetbridge/src/branch/master/LICENSE.artwork