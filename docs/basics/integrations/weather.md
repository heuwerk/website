---
title: Weather providers
---

# Weather providers

To display weather information on your gadget, you will need to install or/and configure a weather provider application.

## Installing a provider

Some options are:

* [Tiny Weather Forecast Germany](#tiny-weather-forecast-germany)
* [Weather notification](#weather-notification)
* [QuickWeather](#quickweather)
* [Breezy Weather](#breezy-weather)
* [OpenWeatherProvider (LineageOS)](#openweatherprovider)

### Tiny Weather Forecast Germany

It also provides non-German data, you can use city/town names in German or a GPS to determine location.

* Install "Tiny Weather Forecast Germany" on [:simple-fdroid: F-Droid.](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany/){: target="_blank" }
* After installing, you must enable the Gadgetbridge support in the app, by navigating to "Settings → Gadgetbridge support".
* Set the location as desired and feel free to adjust the other settings. As city names are in the German language, best is to use the "Find by location" and either write GPS coordinates or enable GPS permission (even just temporarily), and let the app to determine the closest weather stations.

### Weather notification

* Install "Weather notification" on [:simple-fdroid: F-Droid.](https://f-droid.org/packages/ru.gelin.android.weather.notification/){: target="_blank" }
* After installing, you must find & enable the Gadgetbridge skin in the app by scrolling down.
* Also make sure that you have the "Enable notification" enabled in the app.
* Set the location as desired and feel free to adjust the other settings (some of them apply only for Pebble watches, and don't affect other gadgets).
* If you use dynamic location (from GPS) make sure Weather Notification app has access to your location.
* You will also need to add an [OpenWeatherMap API](https://home.openweathermap.org/){: target="_blank" } key.

### QuickWeather

* Install "QuickWeather" on [:simple-fdroid: F-Droid.](https://f-droid.org/packages/com.ominous.quickweather/){: target="_blank" }
* You will need to add an [OpenWeatherMap API](https://home.openweathermap.org/){: target="_blank" } key.
* In the settings, enable the Gadgetbridge extension.

### Breezy Weather

* Install "Breezy Weather" on [:simple-fdroid: F-Droid (IzzyOnDroid repo).](https://apt.izzysoft.de/fdroid/index/apk/org.breezyweather/)
* In "Settings → Widgets & Live wallpaper", click on "Send Gadgetbridge data" and enable "Gadgetbridge".

### OpenWeatherProvider

The following is for LineageOS 16 or older. For 18.1, "Weather Notification" still works with a slight change in setup. Before adding your OpenWeatherMap API key, add your location manually. Then add the API key and the weather should update.

* To use the weather function in LineageOS / CyanogenMod, simply install the OpenWeatherProvider (already installed on CyanogenMod / LineageOS 14 and older) from [this link](https://mirrorbits.lineageos.org/WeatherProviders/OpenWeatherMapWeatherProvider.apk){: target="_blank" } and configure the cLock widget.

* To configure the cLock widget you will need an [OpenWeatherMap API](https://home.openweathermap.org/){: target="_blank" } key.

* Set your API key under: "Apps → Clock → Widget Settings → Weatherpanel → Weather source → OpenWeatherMap" and also set location either from GPS, or as custom location under: "Apps → Clock → Widget Settings → Weatherpanel → Use custom location".

* Then, finally also set a location (does need to be the same as above) in the Gadgetbridge weather settings. The location set in cLock and Gadgetbridge are independent, we cannot query the location from cLock.

## More information for developers

Check [Weather app support](../../internals/development/weather-support.md) to get more details for developers about developing weather apps that work with Gadgetbridge.
