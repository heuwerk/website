---
title: Sleep as Android
---

!!! example "Not yet released"
    Support for this Sleep as Android has been added to Gadgetbridge's codebase, but is not yet released. It is already available in the [nightly releases.](../../index.md#nightly-releases)

Sleep as Android support has been added in {{ 3687|pull }}.

## Supported devices

- [Zepp OS](../topics/zeppos.md) devices
- Pebble devices support a standalone separate integration - see the [PebbleKit](../topics/pebblekit.md#sleep-as-android)

## Configuration

In Gadgetbridge, you must enable the integration in the preferences, as well as select the device that will be used to send data to Sleep as Android.

Sleep as Android has not yet whitelisted Gadgetbridge. In order to interact with Gadgetbridge from Sleep as Android, you must enable the Wearables add-on, and select the DIY option.

Use the package name matching the version of Gadgetbridge you have installed:

| Version | Package |
|:--------|:--------|
| Mainline | `nodomain.freeyourgadget.gadgetbridge` |
| Bangle.js | `com.espruino.gadgetbridge.banglejs` |
| Nightly | `nodomain.freeyourgadget.gadgetbridge.nightly` |
| Nightly Bangle.js | `com.espruino.gadgetbridge.banglejs.nightly` |
| Nightly No Pebble | `nodomain.freeyourgadget.gadgetbridge.nightly_nopebble` |
