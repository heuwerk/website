---
title: Companion device pairing
---

# Companion device pairing

On phones running Android 8.0 (API level 26) and higher, it's possible to use companion device pairing. It is recommended to use this setting, especially on Android 10 and higher as without it for example the "Find phone" feature cannot work correctly.

After the gadget is paired using this method, the gadget can leverage the `REQUEST_COMPANION_RUN_IN_BACKGROUND` permission to start the app from the background. This provides a better way for Gadgetbridge to keep its service running. Read more about background service stability [here](../topics/background-service.md).

In the future there might be a possibility to use companion device pairing to perform a Bluetooth (or Wi-Fi) scan of nearby gadgets, without requiring the `ACCESS_FINE_LOCATION` or `BLUETOOTH_ADMIN` permission. This would minimize the number of permissions needed. Currently, these permissions [are still required](https://developer.android.com/guide/topics/connectivity/companion-device-pairing){: target="_blank" }.

To utilize this in Gadgetbridge, enable "CompanionDevice Pairing" in the settings:

"Settings → Developer Options → Discovery and Pairing options → CompanionDevice Pairing"

And you can now add your gadget to Gadgetbridge.

## Pairing existing devices as companion

For gadgets already paired with Gadgetbridge, you must re-pair (re-bond) them. First, make sure you are currently "Connected" to the gadget as shown in the home page.

Then, go to the "Debug" page from sidebar.

![](../../assets/static/screenshots/sidebar_debug.jpg){: height="600" style="height: 600px;" }

Scroll to the bottom and click "Pair current device as companion".

![](../../assets/static/screenshots/debug.jpg){: height="600" style="height: 600px;" }

Select "OK" in the prompt and done!
