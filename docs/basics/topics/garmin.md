---
title: Garmin watches
---

# Garmin watches

The following list includes all implemented features for Garmin watches. The actual supported features can vary for each device.

--8<-- "blocks.md:non_released_gadget"

!!! warning "Experimental"
    Support for these devices is recent, and still experimental. This page is also still a work in progress and not complete.

List of Garmin watches supported by Gadgetbridge:

<!--
    This is an auto-populated list from gadgets which has "os_garmin" flag,
    see "device_support.yml" file for more details.

    It basically creates a list item for every gadget and
    changes the casing for setting as display name for links.
    (like "garmin_forerunner_245" becomes to "Garmin Forerunner 245")

    Then, an anchor ID appends to the end of the page link,
    so when clicked on it, it will point to the related gadget's own section.
-->
{% for key, device in device_support.items() | sort %}
    {%- if "os_garmin" in device.flags %}
        {% set name = key.replace("_", " ").title() %}
        {% set icon = ' :material-flask-empty-outline:{: title="Experimental" }' if "experimental" in device.flags else "" %}
* [{{ name }}](../../gadgets/wearables/{{ device.vendor }}.md#device__{{ key }}) {{ icon }}
    {% endif -%}
{% endfor %}

### Implemented features

These features are supported by Gadgetbridge and apply to all Garmin watch models included in this page. Note that actual available features per device depends on the device capabilities.

??? note "List of features supported by Gadgetbridge"

    - Set time
    - Device info (firmware, model)
    - Device state (battery, sleep, wearing)
    - Calendar sync
    - Notifications, calls
    - Canned messages for calls and notifications
    - Contacts
    - Send GPS during workouts
    - AGPS upload
    - Activity sync

### Activity Sync

The following fit file types are fetched from the watch and saved in the phone's internal storage:

* Activity (workouts)
* Monitor (daily activity, steps, HR)
* Metrics (not parsed)
* Changelog
* HRV (not parsed)
* Sleep

### Known issues

There are some outstanding issues with the first time a device is paired:

1. You may need to disconnect and reconnect from Gadgetbridge to leave the initial "pairing" screen
2. For some watches where the previous step is not enough, you may need to connect to the official app at least once to become properly initialized (eg. Vivosmart 5 - {{ 3269|issue }}).

!!! note "Open issues"
    Do not forget to check any other [open issues](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues?q=&type=all&sort=&labels=-1238%2c134442&state=open&milestone=0&project=0&assignee=0&poster=0) in the project repository.

### Missing features

- User information
- Settings
- Alarms
- Hydration reminder
- App management

There are more missing features that are not listed here.

### AGPS updates

Watches fetch AGPS updates periodically, by sending HTTP requests directly. Gadgetbridge intercepts these requests and sends a file from the local phone storage. These can be configured from the "Location" page in the device preferences.
