from typing import TYPE_CHECKING, Dict, Any, Optional, Union

if TYPE_CHECKING:
    from mkdocs_macros.plugin import MacrosPlugin

# https://mkdocs-macros-plugin.readthedocs.io/en/latest/
def define_env(env : "MacrosPlugin"):
    widget_config : Dict[str, Dict[str, Any]] = env.variables["widgets"]
    git = widget_config["git_links"]["config"]
    repo_url = env.conf.get("repo_url", "")

    @env.filter
    def issue(number : int, comment : int = None):
        if not widget_config["git_links"].get("enabled", True):
            return \
                "issue #" + str(number) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["issue_comment" if comment else "issue"].format(number),
            link = git["issue_comment_url" if comment else "issue_url"].format(
                number = number,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["issue_hint"].format(number)
        )

    @env.filter
    def pull(number : int, comment : int = None):
        if not widget_config["git_links"].get("enabled", True):
            return \
                "pull request #" + str(number) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["pull_comment" if comment else "pull"].format(number),
            link = git["pull_comment_url" if comment else "pull_url"].format(
                number = number,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["pull_hint"].format(number)
        )

    @env.filter
    def commit(commit_sha : str, comment : str = None):
        if not widget_config["git_links"].get("enabled", True):
            return \
                "commit #" + str(commit_sha) + \
                ("" if not comment else "#" + str(comment))
        return '[{name}]({link}){{ title="{title}" target="_blank" }}'.format(
            name = git["commit"].format(commit_sha[:10]),
            link = git["commit_url"].format(
                commit_sha = commit_sha,
                comment = comment,
                repo_url = repo_url
            ),
            title = git["commit_hint"].format(commit_sha[:10])
        )

    @env.filter
    def color(color : str):
        color_config = widget_config["color"]
        # If widget has disabled, only render it as a inline code block.
        if not color_config.get("enabled", True):
            return f"`{color.upper()}`"
        return (
            '<span data-clipboard-text="{color}">'
            '<span class="{class_name}" style="background-color: {color};"></span>'
            '`{color}`</span>'
        ).format(
            class_name = color_config["class_name"],
            color = color if not color.startswith("#") else color.upper()
        )

    @env.macro
    def file_listing():
        result = []
        for child in env.page.parent.children:
            filename = child.file.src_uri.split('/')[-1]
            title = filename[:-3].replace("-", " ").title()
            # Exclude current page.
            if child == env.page:
                continue
            result.append(f"* [{title}]({filename})")
        return \
            "Pages under this section:\n\n" + "\n".join(result)

    def render_tag(
        class_name : str,
        description : str,
        icon : str,
        name : str,
        extra : Union[str, bool] = False,
        link : Optional[str] = None,
        copy_text : Optional[str] = None
    ):
        tag = '<span class="{class_name}" title="{title}"{attr}>:{icon}: {name}{extra}</span>'.format( # noqa: E501
            class_name = class_name,
            title = description,
            icon = "" if not icon else icon.replace("/", "-", 1),
            name = name,
            extra = extra if isinstance(extra, str) else \
                ' :material-launch:{: style="margin: 0 0 0 10px !important;" }' \
                if ((extra is True) and link) else "",
            attr = "" if not copy_text else f' data-clipboard-text="{copy_text}"'
        )
        if link:
            return '<a href="{href}">{tag}</a>'.format(href = link, tag = tag)
        return tag

    @env.macro
    def device(device_id : str):
        label_config = widget_config["label"]
        link_names = label_config["links"]
        header_id = "device__" + device_id
        # If widget has disabled, don't render it.
        if not label_config.get("enabled", True):
            return ""
        device = env.variables["device_support"][device_id]
        out = ""
        for i in device["flags"]:
            f = label_config["values"][i]
            if "hidden" in f:
                continue
            out += render_tag(
                class_name = f["class_name"], 
                description = f.get("description", ""), 
                icon = f.get("icon", ""), 
                name = f["name"], 
                extra = True, 
                link = f.get("link", None)
            )
        for k, v in device.get("links", {}).items():
            f = link_names[k]
            out += render_tag(
                class_name = f["class_name"], 
                description = f.get("description", ""), 
                icon = f.get("icon", ""), 
                name = f.get("name", ""), 
                extra = None, 
                link = v
            )
        return \
            "{{: #{0} }}".format(header_id) + "\n\n" + \
            f'<span class="{label_config["class_name"]}">' + out + '</span>'

    @env.macro
    def get_gadget_breakdown():
        stats = [0, 0, 0, 0, 0, []]
        for _, i in env.variables["device_support"].items():
            stats[0] += 1
            if ("feature_high" in i["flags"]) or ("feature_most" in i["flags"]):
                stats[1] += 1
            elif "feature_partial" in i["flags"]:
                stats[2] += 1
            elif "feature_poor" in i["flags"]:
                stats[3] += 1
            else:
                stats[4] += 1
            if i["vendor"] not in stats[5]:
                stats[5].append(i["vendor"])
        return {
            "all": str(stats[0]), # It needs to be str as it will be splitted to digits.
            "good": stats[1],
            "meh": stats[2],
            "poor": stats[3],
            "unknown": stats[4],
            "vendor": len(stats[5])
        }