---
title: Bose
---

## QC35 {{ device("bose_qc35") }}

Support for this gadget was added in {{ 2520|pull }}.
