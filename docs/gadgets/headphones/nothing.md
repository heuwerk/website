---
title: Nothing
---

# Nothing

## Ear (1) {{ device("nothing_ear_1") }}

Supported features by Gadgetbridge:

* Find device
* Read battery level
* Set to pause/play based on in-ear detection
* Setting audio profiles
    * ANC (Automatic Noise Canceling)
    * ANC-Lite
    * Transparency (Ambient sounds)
    * Off (No canceling, no ambient)

## Ear (2) {{ device("nothing_ear_2") }}

Added from feedback on {{ 3428|issue }}.

The same features supported by Ear (1) should apply to the Ear (2).

## Ear (Stick) {{ device("nothing_ear_stick") }}

The same features supported by Ear (1) should apply to the Ear (Stick), with a different set of audio profiles (only supports ANC and off).
