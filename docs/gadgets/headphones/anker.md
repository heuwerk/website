---
title: Anker
---

## Soundcore Liberty 3 Pro {{ device("anker_soundcore_liberty_3_pro") }}

Support for this gadget was added in {{ 3753|pull }}.

### Working features

* Connection
* Firmware version and serial number
* Display battery life when sent by earbuds
* Set noise cancelling and transparency
* Update noise cancelling / transparency setting in Gadgetbridge when changed on the earbuds
* Set the behavior of touch actions

### Missing features

* Hear-Id (calibration of ANC for your ears)
* Equalizer
* Actively requesting the battery-life of the earbuds, so it may get out-of-date
