---
title: Garmin
---

# Garmin

--8<-- "blocks.md:non_released_gadget"

Gadgetbridge supports some Garmin devices, listed below. It's very likely that other devices are supported, you can try to use [Pairing unsupported gadgets](../../basics/pairing/index.md#pairing-unsupported-gadgets) to pair an unsupported Garmin watch. If it works, please please [open an issue](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/new?template=.gitea%2fissue_template%2fdevice_request.yml) or reach out on [Matrix](https://app.element.io/#/room/#gadgetbridge:matrix.org) so we can enable official support for it.

See the [Garmin](../../basics/topics/garmin.md) page for a list of features and issues common to all Garmin watches.

!!! warning "Warning"
    * We have noticed that in some cases the Bluetooth traffic contains some bits of information (`oauth_token`, `oauth_consumer_key`, `oauth_signature`, ...) that might (potentially, we did not test) grant access to the user's garmin account. **Therefore we warn all the users to mind this information when sharing the logs.**
    * User data/configuration is unmanaged, which means that whatever was set in the official app will remain untouched, no matter what is set in Gadgetbridge.

### Forerunner 245 {{ device("garmin_forerunner_245") }}

### Instinct 2 Solar {{ device("garmin_instinct_2_solar") }}

### Instinct 2 Solar - Tactical Edition {{ device("garmin_instinct_2_soltac") }}

### Instinct 2S {{ device("garmin_instinct_2s") }}

### Instinct 2S Solar {{ device("garmin_instinct_2s_solar") }}

Added in {{ 3805|pull }}.

### Instinct 2X Solar {{ device("garmin_instinct_2x_solar") }}

Added based on feedback from {{ 3180|pull }}.

### Instinct Crossover {{ device("garmin_instinct_crossover") }}

### Instinct Solar {{ device("garmin_instinct_solar") }}

### Swim 2 {{ device("garmin_swim_2") }}

### Venu 2 Plus {{ device("garmin_venu_2_plus") }}

### Venu 3 {{ device("garmin_venu_3") }}

### Vívoactive 4 {{ device("garmin_vivoactive_4") }}

### Vívoactive 4s {{ device("garmin_vivoactive_4s") }}

### Vívoactive 5 {{ device("garmin_vivoactive_5") }}

### Vívomove HR {{ device("garmin_vivomove_hr") }}

Support for this gadget was originally added in {{ 3180|pull }}. However, due to the necessary changes for newer devices to work, this support was replaced in {{ 3782|pull }}, which requires a firmware upgrade.

Data synced while using the old version is still in Gadgetbridge database, but will not be displayed in the UI. If you own a vivomove HR and were using Gadgetbridge with it before these changes, please [open an issue](https://codeberg.org/Freeyourgadget/Gadgetbridge/issues/new?template=.gitea%2fissue_template%2fbug_report.yml) or reach out to us on [Matrix](https://app.element.io/#/room/#gadgetbridge:matrix.org) in the main chat room.

### Vívomove Style {{ device("garmin_vivomove_style") }}
