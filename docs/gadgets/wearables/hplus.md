---
title: HPlus
---

# HPlus

This applies to all of Zeblaze Zeband, Zeband Plus, Makibes F68, and many others, as they share most of the same protocol. Other probably supported gadgets are S200, HPlus H4, H5, H9 and K8.

Most devices provide the same features, although the external appearance may be quite different. The variations in the protocol are minimal. More recent versions will have support for Unicode characters, while older versions will only support GB2312 characters. Hence, accented characters will be displayed as Chinese symbols. We workaround this the best we know.

## Zeblaze Zeband {{ device("hplus_zeblaze_zeband") }}

This gadget will not display all accented characters correctly.

Supported features by Gadgetbridge:

* Discovery and pairing
* Setting weight, height, age, date, time, day of week
* Synchronize activity data: all stored not all is displayed
* Notifications (Display + vibration) with differentiation between phone calls and messages
* Display firmware version and battery status
* "Real time" (10m) heart rate monitoring: the devices do not seem to provide true real time values.
* Sleep monitoring: individual sleep phases stored but not presented. Only light and deep is presented
* Calories and distance stored but not presented
* Real time steps
* Alarms
* Configuration of some specific settings: Language, Real time HR State, Goals, Screen time.

Missing features:

* Inactivity alarms: messages are known. Requires some more code.
* Configuration of some specific settings: (HR thresholds, inactivity alarms, enable/disable SMS and phone notifications in the device, find me).
* Swimming statistics: Needs tinkering with the devices as messages are unknown.
* Activity only considers HR... So there is no activity charts if All Day HR monitoring is disabled.
* Firmware update: Standard Nordic DFU process. No code developed.

Development/work planned/ideas to be explored:

* Find if there is a configurable HR monitoring interval
* Find how inactivity timers work
* Some messages are not understood and require further tinkering
* Temporarily enable All Day HR Monitoring, for 1-2 seconds, in order to get latest data from band. This would be a simple synchronization method for steps, distance and calories.

## Zeblaze Zeband Plus {{ device("hplus_zeblaze_zeband_plus") }}

Given details under [Zeblaze Zeband](#device__hplus_zeblaze_zeband) also apply to this gadget.

## Makibes F68 {{ device("hplus_makibes_f68") }}

Given details under [Zeblaze Zeband](#device__hplus_zeblaze_zeband) also apply to this gadget.
