---
title: Shell Supercars Protocol
---

# Shell Supercars Protocol

* Wireshark dissector for the protocol is [here](https://codeberg.org/Freeyourgadget/Gadgetbridge-tools/src/branch/main/supercars/wireshark){: target="_blank" }.
* Protocol description and encryption, BLE module description [here](https://gist.github.com/scrool/e79d6a4cb50c26499746f4fe473b3768){: target="_blank" }.
