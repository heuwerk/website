---
title: Amazfit Bip Firmware Update
---

# Amazfit Bip Firmware Update

This page is about the Amazfit Bip (1), Model A1608.

--8<-- "blocks.md:firmware_danger"

However we had one brick caused by the faulty beta firmware 0.1.0.51[^3]. Over the air update.

## Getting the firmware

Since we may not distribute the firmware, you have to do a little work. You need to find and download a Mi Fit APK file. There is an APK Mirror Web site that might help you find Mi Fit. Extract the downloaded .apk file with "unzip", and you will find an `assets/` directory containing `*.fw` files, among others. As of Mi Fit 4.0.1, the apk dropped all included firmware files except for Mi Bands, Shoes and Scales, resulting in OTA.

## OTA

Over the Air update.

As of Mi Fit 4.0.1, the apk dropped all included firmware files except for Mi Bands, Shoes and Scales, resulting in OTA (over the air) firmware distribution. This means that you will need to search for the files online, amazfitwatchfaces can be one of the sources (be aware - i am intentionally not adding a link here). During OTA, Mi Fit stores the update files temporarily in `/data/data/com.xiaomi.hm.health/files/firmware/xx` directory.

The Amazfit Bip requires the `Mili_chaohu.*` files. For **tested versions** and **known issues** check [below](#known-firmware-versions).

## Installing the firmware

Copy the desired Amazfit Bip gps, resource and firmware files as a `*.gps`, `*.res` and `*.fw` file to your Android device and open it from any file manager on that device. The Gadgetbridge FW/App Installer activity should then be started and guide you through the installation process.

### Recommended flashing order

1. `Mili_chaohu.fw`
2. `Mili_chaohu.ft`
3. `Mili_chaohu.res` (the gadget will tell you when needed)
4. `Mili_dth.gps` (this rarely changes, check the table below, if you should update)

Flashing the `*.fw` triggers a reboot and will show a screen which will ask you to flash the `.res` if your version is outdated.

**Note 1:** Both upgrade and downgrade of firmware versions is possible but untested, we do not encourage to do it.

**Note 2:** After flashing to a 0.0.9.x release the first time coming from prior versions you have to remove the pairing in Android Bluetooth settings, then press the + button in Gadgetbridge to pair again.

**Note 3:** Starting with Firmware version 0.0.9.14 the Amazfit Bip supports English menus. If the desired language is not shown  automatically, you can select it in "Settings → Amazfit Bip Settings → Language" after installing the corresponding firmware file.

**Note 4:** Recent firmwares come in two version ("Normal" and Latin), While "Normal" seems to support English and Chinese (Spanish support introduced and later dropped), the Latin version seems to support Spanish and Russian. If you flash the Latin version you also need to flash the Latin font (which does not contain CJK characters).

**Note 5:** The GPS firmware files are a bit of a mess. Usually one would expect that the correct GPS file is the Mili_chaohu.gps, but in assets/firmwares.json one can see that the Mili_dth.gps is the recommended one.

**Note 6:** If you want to install the latin version (Languages from 1.1.5.36: Spanish, Russian, French, German, Italian, Turkish) please choose the files ending with ".latin" if any.

**Note 7:** Any time you flash from latin to non-latin version or vice versa you need to flash the font file (`.ft` / `.ft.latin`), also. You may skip step 2 otherwise.

## Known firmware versions

Original Firmware Versions:

- 0.0.7.90 (Tested, pre-installed with Chinese watches manufactured July 2017)
- 0.0.8.95 (pre-installed with the "English Version" manufactured December 2017)

### 0.0.8 series

fw ver    | MiFit ver | tested | known issues | res ver | gps ver | fw-md5 | res-md5 | gps-md5
----------|-----------|--------|-------------------|---------|---------|--------|---------|--------
0.0.8.10  | 3.0.0     | no     | ??           | 7 | 9367,8f79a91,0,0, | d5e10b1b25f9a2a4cabba0ca8ff64b87 | 2283a4d78058321c6eed60ea17dc83b1 | db27b914056153ff47f137fd0f91209e
0.0.8.20  | 3.0.2     | no     | ??           | 7 | 9367,8f79a91,0,0, | d737c210d960ac552dba9e3d88d96a3e | 2283a4d78058321c6eed60ea17dc83b1 | db27b914056153ff47f137fd0f91209e
0.0.8.32  | 3.0.4     | no     | ??           | 10 | 9367,8f79a91,0,0, | 2e20c581bad02f849b1c7ddf9d2beb94 | ddc3c7075de22e8a82229a5d4e660532 | db27b914056153ff47f137fd0f91209e
0.0.8.74  | 3.0.5     | yes    | [deep sleep](#fn:1) | 12 | 9367,8f79a91,0,0, | bc0eccb54246a999ceb0052ed0f542d8 | 88a6675421ae9a58b2d7b85a8782842d | db27b914056153ff47f137fd0f91209e
0.0.8.88  | 3.0.7     | yes    | slow UI, [deep sleep](#fn:1) | 13 | 9367,8f79a91,0,0, | 2d182f06402b7bb9afe591f2697d312f | 8c2953fb1d714b0fe64c4013dd033bfb | db27b914056153ff47f137fd0f91209e
0.0.8.96  | OTA only  | yes    | [deep sleep](#fn:1) | 16 | 9367,8f79a91,0,0, | 5458007fe89a3e4df2d166d49d2a4d9b | 2a745c9e97a561bff8472f2193086d52 | db27b914056153ff47f137fd0f91209e
0.0.8.97  | OTA only  | yes    | [deep sleep](#fn:1) | 16 | 9367,8f79a91,0,0, | e19cf338204b9190b88f5666399d66b5 | 2a745c9e97a561bff8472f2193086d52 | db27b914056153ff47f137fd0f91209e
0.0.8.98  | OTA only  | yes    | [deep sleep](#fn:1) | 16 | 9367,8f79a91,0,0, | c2c5737a304b476e197ea38354b81ea8 | 2a745c9e97a561bff8472f2193086d52 | db27b914056153ff47f137fd0f91209e

### 0.0.9 series (introduces English language)

fw ver  | MiFit ver | tested | known issues | res ver | gps ver | fw-md5 | res-md5 | gps-md5
---------|-----------|--------|-------------------|---------|---------|--------|---------|--------
0.0.9.14 | 3.1.0     | yes    | [deep sleep](#fn:1) | 17 | 9367,8f79a91,0,0, | 92824f9e7cbb1a0729fbd27938ab2ba5 | e90b394bf0f9a055a108798656877ebe | db27b914056153ff47f137fd0f91209e
0.0.9.26 | 3.1.2     | yes    | [deep sleep](#fn:1), connection drops with some phones | 17 | 9565,dfbd8fa,0,0, | 78e59e39d237198af0c0e2aed5c82a1e | e90b394bf0f9a055a108798656877ebe | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.0.9.40 | 3.1.3     | yes    | [deep sleep](#fn:1) | 19 | 9565,dfbd8fa,0,0, | fae9548f699ede59687b219a20e6e70d | 7099605b7e062645476f6b8bb815f6fb | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.0.9.49 [(beta)](#fn:2) | 3.1.6.1   | yes    | [deep sleep](#fn:1) | 20 | 9565,dfbd8fa,0,0, | ba17b217a85d5e48e7061f36d9e9554e | 656c784e54c9ece7688eea64cb4d32d3 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.0.9.59 [(beta)](#fn:2) | 3.1.7.1   | yes    | [deep sleep](#fn:1) | 20 | 9565,dfbd8fa,0,0, | 617af082c8526b35452702798e6ce33c | 656c784e54c9ece7688eea64cb4d32d3 | 97f9794cc46b2ebddaa0b52fe27a4f8f

### 0.1.0 series

fw ver  | MiFit ver | tested | known issues | res ver | gps ver | fw-md5 | res-md5 | gps-md5
---------|-----------|--------|-------------------|---------|---------|--------|---------|--------
0.1.0.08 | 3.1.9     | yes     | [deep sleep](#fn:1) | 20 | 9565,dfbd8fa,0,0, | 47ae3eb87462a946deddc315be00b406 | 656c784e54c9ece7688eea64cb4d32d3 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.11 [(beta)](#fn:2) | 3.1.8.1   | yes    | [deep sleep](#fn:1) | 20 | 9565,dfbd8fa,0,0, | 52e056e27a5b27891e257b71dae39e09 | 656c784e54c9ece7688eea64cb4d32d3 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.17 [(beta)](#fn:2) | 3.2.0.1   | yes     | [deep sleep](#fn:1) | 21 | 9565,dfbd8fa,0,0, | 15c899aff4842eaea3608b512e86b2c6 | fcda343cdffbe12acec6bb8e9e9d20ca | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.26    | 3.2.1     | yes    | [deep sleep](#fn:1) | 22 | 9565,dfbd8fa,0,0, | a64b9ce5d58612d13da08b507db79a01 | c9e82528cb97db2e5bb85781d6f38c54 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.27 [(beta)](#fn:2) | 3.2.2.1   | yes    | [deep sleep](#fn:1) | 22 | 9565,dfbd8fa,0,0, | f76b8c0e536c609ee7e04400f3f866ed | c9e82528cb97db2e5bb85781d6f38c54 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.33 [(beta)](#fn:2) | 3.2.2.2   | yes    | [deep sleep](#fn:1) | 23 | 9565,dfbd8faf42,0 | 3109ebb17d7bfee045e1996d79030aad | 136a07f0f1740d3a9cd3688e50500d44 | b4f787b3e722e69252df90e4c710b85d
0.1.0.39 [(beta)](#fn:2) | 3.2.2.3   | yes    | [deep sleep](#fn:1) | 24 | 9565,dfbd8faf42,0 | c37c04794ffc893c872cc116a402aaf4 | 25e306b149e5a0f574b81521a7ad6951 | b4f787b3e722e69252df90e4c710b85d
0.1.0.43 [(beta)](#fn:2) | 3.2.3.1   | yes    | [deep sleep](#fn:1) | 24 | 9565,dfbd8faf42,0 | 19d3adcf6583a76f4d10a32a8276b022 | 25e306b149e5a0f574b81521a7ad6951 | b4f787b3e722e69252df90e4c710b85d
0.1.0.44    | 3.2.5     | yes     | [deep sleep](#fn:1) | 24 | 9565,dfbd8fa,0,0, | c2deae493b880e50feae1c4a8953f665 | 25e306b149e5a0f574b81521a7ad6951 | 97f9794cc46b2ebddaa0b52fe27a4f8f
0.1.0.45 [(beta)](#fn:2) | 3.2.5.1   | yes    | [deep sleep](#fn:1) | 24 | 9567,8b05506,0,0, | 9c1e36695cae0d8711a2aa9f4990aea9 | 25e306b149e5a0f574b81521a7ad6951 | c426b761147dd871e22fb887a8de630f
0.1.0.51 [(beta)](#fn:2) | 3.2.7.1   | **[brick](#fn:3)** | [deep sleep](#fn:1) | 25 | 9567,8b05506,0,0, | 26116b3b2ebb4b83badf92fa5814ff35 | 48a0d11de49f52b12cc2c8a7a72f6218 | c426b761147dd871e22fb887a8de630f
0.1.0.66 [(beta)](#fn:2) | 3.2.7.2   | no     | [deep sleep](#fn:1), slow UI | 26 | 9567,8b05506,0,0, | e00e197f9f9c83ea15e1083eaf6e8814 | e77e6ec609a6fedf5f8954a2f00011de | c426b761147dd871e22fb887a8de630f
0.1.0.70    | 3.2.7     | yes     | [deep sleep](#fn:1) | 26 | 9567,8b05506,0,0, | 7fbbfba40b8c26fa086704843747edee | e77e6ec609a6fedf5f8954a2f00011de | c426b761147dd871e22fb887a8de630f
0.1.0.77 [(beta)](#fn:2) | 3.2.8.1   | no     | [deep sleep](#fn:1) | 27 | 9567,8b05506,0,0, | 585b8ff7eddade8d403816239b5b5ad5 | 57733612f256814ab190c6ea244d2035 | c426b761147dd871e22fb887a8de630f
0.1.0.80    | 3.2.8     | yes    | [deep sleep](#fn:1) | 27 | 9567,8b05506,0,0, | 769391c599e1091b0b07dbb23ee33a92 | 57733612f256814ab190c6ea244d2035 | c426b761147dd871e22fb887a8de630f
0.1.0.86    | 3.2.9     | yes    | [deep sleep](#fn:1) | 28 | 9567,8b05506,0,0, | 787d8bb255bd7515a75bd194bbdf432c | f759680274f023d057d8f529b27fb0f7 | c426b761147dd871e22fb887a8de630f
0.1.0.87 [(beta)](#fn:2) | 3.3.0.1   | no     | [deep sleep](#fn:1) | 28 | 9567,8b05506,0,0, | 73a121307977235f4cb3f4a21e866421 | f759680274f023d057d8f529b27fb0f7 | c426b761147dd871e22fb887a8de630f
0.1.0.89 [(beta)](#fn:2) | 3.3.0.2   | no     | [deep sleep](#fn:1) | 28 | 9567,8b05506,0,0, | 503fe62959df4ccc1755e7b5147d59c7 | f759680274f023d057d8f529b27fb0f7 | c426b761147dd871e22fb887a8de630f
0.1.0.99 [(beta)](#fn:2) | 3.3.2.1   | no     | [deep sleep](#fn:1) | 29 | 9567,8b05506,0,0, | d00c5dbe4c1cff2de0904e9f66fd752b | bfe0e5550f3e6628c2ea11e3c08978d5 | c426b761147dd871e22fb887a8de630f


### 0.1.1 series

fw ver  | MiFit ver | tested | known issues | res ver | gps ver | fw-md5 | res-md5 | gps-md5
---------|-----------|--------|-------------------|---------|---------|--------|---------|--------
0.1.1.05 [(beta)](#fn:2) | 3.3.2.2   | no     | [deep sleep](#fn:1) | 30 | 9567,8b05506,0,0, | 7e4e4163f6bcb965f336f9f81d644b44 | 4648ed16f3f06ba4af189f5d2f9a2e15 | c426b761147dd871e22fb887a8de630f
0.1.1.14    | 3.3.1     | yes    | [deep sleep](#fn:1) | 31 | 9567,8b05506,0,0, | 0b45fc881e5396614488226b995d4ee6 | 16542e0167d0cb6a2e0be94736af7383 | c426b761147dd871e22fb887a8de630f
0.1.1.15 [(beta)](#fn:2) | 3.3.4.1   | no     | [deep sleep](#fn:1) | 32 | 9567,8b05506,0,0, | ce77cade5a9b4a61bc0dfdbfba74ebc1 | e8d954081b4448ce0621caa35ba59c54 | c426b761147dd871e22fb887a8de630f
0.1.1.17 [(beta)](#fn:2) | 3.3.4.6   | yes    | [deep sleep](#fn:1) | 32 | 9567,8b05506,0,0, | 330b77a0cf4f0dd1500581fc57543ac7 | e8d954081b4448ce0621caa35ba59c54 | c426b761147dd871e22fb887a8de630f
0.1.1.25 [(beta)](#fn:2) | 3.3.4.9   | no     | [deep sleep](#fn:1) | 32 | 9567,8b05506,0,0, | fca925d9979336d91bf2defebae5061e | e8d954081b4448ce0621caa35ba59c54 | c426b761147dd871e22fb887a8de630f
0.1.1.29 [(beta)](#fn:2) | 3.3.4.10  | yes    | [deep sleep](#fn:1) | 33 | 9567,8b05506,0,0, | 591645d7043b7ed8fd195595a350baad | e7521511a24bf7ee87f4262dfe04d9c8 | c426b761147dd871e22fb887a8de630f
0.1.1.31 [(beta)](#fn:2) | 3.3.4.11  | yes    | [deep sleep](#fn:1) | 34 | 9567,8b05506,0,0, | bb5b6add4950b848201575f71c73aa42 | 5c78d121088725c9be6a3f58333cd464 | c426b761147dd871e22fb887a8de630f
0.1.1.36    | 3.3.2     | yes    | [deep sleep](#fn:1) | 34 | 9567,8b05506,0,0, | 958afb6ed1d21e028554b525d294667c | 5c78d121088725c9be6a3f58333cd464 | c426b761147dd871e22fb887a8de630f
0.1.1.39 [(beta)](#fn:2) | 3.3.4.13  | yes    | [deep sleep](#fn:1) | 36 | 9567,8b05506,0,0, | a6b1425f0ac0a0777bc474a66256e548 | 920d9e961e7f770bdd57dc9aeeba7f5f | c426b761147dd871e22fb887a8de630f
0.1.1.41 [(beta)](#fn:2) | 3.4.0.1   | yes    | [deep sleep](#fn:1) | 37 | 9567,8b05506,0,0, | be8d0cee6ccdc89b00c6696229be2083 | 2a092f2bd12410c587dd6a4b26e7a396 | c426b761147dd871e22fb887a8de630f
0.1.1.45 [(beta)](#fn:2) | 3.4.0.2   | yes    | [deep sleep](#fn:1) | 38 | 9567,8b05506,0,0, | 8db0857956bff4446277418ca5a54db8 | b93943ea00af4f1d4d17434a4b3a9c67 | c426b761147dd871e22fb887a8de630f

### 1.x.x series

| fw ver        | fw type   | MiFit ver | tested | known issues | res ver | gps ver | fw-md5
| --------------|-----------|-----------|--------|--------------|---------|---------|-------
| 1.0.1.00    | ?         | 3.4.2     | yes    | [deep sleep](#fn:1) | 39 | 9567,8b05506,0,0, | `2e4d0a04b7c6a58bcebdb6643a164a5c`
| 1.0.2.00 [(beta)](#fn:2) | ?         | 3.4.4.1   | yes    | [deep sleep](#fn:1) | 40 | 9567,8b05506,0,0, | `37ed4544d7b67ae9af2359d6f4583ae0`
| 1.1.1.00 [(beta)](#fn:2) | ?         | 3.4.4.2   | yes    | [deep sleep](#fn:1), slowed a bit | 40 | 9567,8b05506,0,0, | `020d5b055cc32aa1f374641b10388661`
| 1.1.2.05    | non-latin | 3.4.4     | yes    | [deep sleep](#fn:1) | 41 | 9567,8b05506,0,0, | `21edab6ad18108817409107bc5425e36`
| 1.1.2.05    | latin     | 3.4.4     | yes    | [deep sleep](#fn:1) | 41 | 9567,8b05506,0,0, | `381b23c23916435e72cf8af354b786c2`
| 1.1.5.02    | non-latin | 3.5.3     | yes    | [deep sleep](#fn:1) | 42 | 15974,e61dd16,126 | `42ded708876938608d0496eac96dc96e`
| 1.1.5.02    | latin     | 3.5.3     | yes    | [deep sleep](#fn:1) | 42 | 15974,e61dd16,126 | `e036171337c8f437c5fd61197af65dcf`
| 1.1.5.04    | non-latin | 3.5.3     | yes    | [deep sleep](#fn:1) | 42 | 15974,e61dd16,126 | `fd7d4f4108d3a8af88c8ee1adc75c6e3`
| 1.1.5.04    | latin     | 3.5.3     | yes    | [deep sleep](#fn:1) | 42 | 15974,e61dd16,126 | `8ee90bef6f7c9ddb336680d5c0202b5f`
| 1.1.5.12    | non-latin | 3.5.8     | yes    | ? | 42 | 15974,e61dd16,126 | `a3cf74275f620b562df030a29b070aed`
| 1.1.5.12    | latin     | 3.5.8     | yes    | ? | 42 | 15974,e61dd16,126 | `95978e16fa015422ae22d911404c3f75`
| 1.1.5.16    | non-latin | 3.5.9     | yes    | ? | 42 | 15974,e61dd16,126 | `1196c0164f9b313fd58892dcb636f783`
| 1.1.5.16    | latin     | 3.5.9     | yes    | ? | 42 | 15974,e61dd16,126 | `2c38eca0c09c86c0d6ab0b67bf6fe30f`
| 1.1.5.24    | non-latin | OTA       | yes    | ? | 61 | 15974,e61dd16,126 | `560465755e9ef34a054189e5a8fdeca3`
| 1.1.5.36    | latin     | OTA       | yes    | ? | 61 | 15974,e61dd16,126 | `8c0d17f07928ce316a5c392104e6edeb`
| 1.1.5.56    | non-latin | OTA       | yes    | ? | 69 | 18344,eb2f43f,126 | `0d28a72b948d61c562570d240daa86c2`
| 1.1.5.56    | latin     | OTA       | no     | ? | 69 | 18344,eb2f43f,126 | `b3bc16b71eb30747c760c2df4c033048`
| 1.1.6.30    | latin     | OTA       | no     | ? | 83 | 18344,eb2f43f,126 | `845aa68a28f0c7cc772a8e7854ede36d`
| 1.1.6.32    | latin     | OTA       | no     | ? | 83 | 18344,eb2f43f,126 | `73f6721a25afeb02f2ab1b35600d663a`
| 1.1.6.34    | non-latin | OTA       | no     | ? | 84 | 18344,eb2f43f,126 | `005a7e0c7e0524e5aa7c298d9f220430`
| 1.1.6.36    | latin     | OTA       | no     | ? | 84 | 18344,eb2f43f,126 | `8fe059039de9176e34142b4b37bf6993`
| 1.1.6.48    | latin     | OTA       | no     | ? | 85 | 18344,eb2f43f,126 | `984e08969abb5faa3275c9fe25f608d9`
| 1.1.7.02    | latin     | OTA       | no     | ? | 86 | 18344,eb2f43f,126 | `f8ba89ce321dfe370ee54960faaf2856`

## Resource files

| Version | md5                                |
|---------|------------------------------------|
| 7       | `2283a4d78058321c6eed60ea17dc83b1` |
| 10      | `ddc3c7075de22e8a82229a5d4e660532` |
| 12      | `88a6675421ae9a58b2d7b85a8782842d` |
| 13      | `8c2953fb1d714b0fe64c4013dd033bfb` |
| 16      | `2a745c9e97a561bff8472f2193086d52` |
| 17      | `e90b394bf0f9a055a108798656877ebe` |
| 19      | `7099605b7e062645476f6b8bb815f6fb` |
| 20      | `656c784e54c9ece7688eea64cb4d32d3` |
| 21      | `fcda343cdffbe12acec6bb8e9e9d20ca` |
| 22      | `c9e82528cb97db2e5bb85781d6f38c54` |
| 23      | `136a07f0f1740d3a9cd3688e50500d44` |
| 24      | `25e306b149e5a0f574b81521a7ad6951` |
| 25      | `48a0d11de49f52b12cc2c8a7a72f6218` |
| 26      | `e77e6ec609a6fedf5f8954a2f00011de` |
| 27      | `57733612f256814ab190c6ea244d2035` |
| 28      | `f759680274f023d057d8f529b27fb0f7` |
| 29      | `bfe0e5550f3e6628c2ea11e3c08978d5` |
| 30      | `4648ed16f3f06ba4af189f5d2f9a2e15` |
| 31      | `16542e0167d0cb6a2e0be94736af7383` |
| 32      | `e8d954081b4448ce0621caa35ba59c54` |
| 33      | `e7521511a24bf7ee87f4262dfe04d9c8` |
| 38      | `b93943ea00af4f1d4d17434a4b3a9c67` |
| 39      | `de29d2348bef88ce81fa307b2b5adddb` |
| 40      | `1bd4015d3e4beccd6f20e67404e0799d` |
| 41      | `c49e723e02f8ba27682641bc473b3519` |
| 42      | `01e1408f4cc42e6c428b12d586675463` |
| 61      | `e7c9e596adbefc8d652b1bd8d63f0d81` |
| 69      | `5b6990d4733329fd8998f6cad81332d1` |
| 83      | `ae4a36159cc076954610627709c98f35` |
| 84      | `d9a0b2040a20276941af82fcf23fa1e1` |
| 85      | `b98aeee6b3e5e75fe359f97a1f60da9e` |
| 86      | `05f53bea6436668fe3842ec494b086f5` |

## GPS files

| Version           | md5                                |
|-------------------|------------------------------------|
| 9367,8f79a91,0,0, | `db27b914056153ff47f137fd0f91209e` |
| 9565,dfbd8fa,0,0, | `97f9794cc46b2ebddaa0b52fe27a4f8f` |
| 9567,8b05506,0,0, | `c426b761147dd871e22fb887a8de630f` |
| 15974,e61dd16,126 | `decbf6ac1bd747a9c78dbe00d77e9652` |
| 18344,eb2f43f,126 | `228366f374fa3cf82a98ddcb0647fbd8` |

## Firmware types

### Latin

Font grid size: 16x9

**Included languages**

* Italian
* French
* Turkish
* Spanish (default)
* Russian
* Portuguese
* English (from 1.1.6.48)

### Non-Latin

Font grid size: 16x16

**Included languages**

* Simplified Chinese
* Traditional Chinese
* English (default)

## Colors

Here is the color palette supported by the Bip (note that the Bip Lite and Bip S support a wider range of color).

| Color                 | Name          |
|-----------------------|---------------|
| {{ "#000000"|color }} | Black         |
| {{ "#0000FF"|color }} | Blue          |
| {{ "#00FF00"|color }} | Green         |
| {{ "#FF0000"|color }} | Red           |
| {{ "#00FFFF"|color }} | Cyan          |
| {{ "#FF00FF"|color }} | Magenta       |
| {{ "#FFFF00"|color }} | Yellow        |
| {{ "#FFFFFF"|color }} | White         |
| {{ "#FEFE00"|color }} | *Transparent* |

## See also

* <https://github.com/amazfitbip/>{: target="_blank" }

[^1]: Deep sleep detection does not work properly with Gadgetbridge, see {{ 686|issue(343773224) }}.
[^2]: Beta firmware (never appeared in a regular Mi Fit release).
[^3]: We had a brick on this firmware, a bootloop occurred without any way to fix it. This has also been [confirmed here.](https://4pda.ru/forum/index.php?s=&showtopic=835724&view=findpost&p=63329141){: target="_blank" }
