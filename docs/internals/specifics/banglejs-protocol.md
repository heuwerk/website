---
title: Bangle.js Protocol
---

# Bangle.js Protocol

The Bangle.js integration for Gadgetbridge sends JSON-formatted packets over a 'Nordic UART' serial service on Bluetooth LE.

Sent JSON is wrapped in `GB({ ... })\n` so that a function called `GB` on Bangle.js can be called via the REPL, and received data is sent as raw newline-separated JSON.

More info on the packet formats (and usage) can be found on [Espruino website.](https://www.espruino.com/Gadgetbridge){: target="_blank" }
