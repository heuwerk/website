---
title: Automations
icon: material/flash
---

# Automations

This category contains information related to automation. Examples of automation are:

* performing on-watch functions automatically by sending a signal from the phone
* performing phone functions automatically by sending a signal from the watch
* changing Gadgetbridge device settings from automation apps like Tasker, Automate, MacroDroid, etc.

{{ file_listing() }}
