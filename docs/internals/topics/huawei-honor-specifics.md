---
title: Huawei and Honor specifics
---

# Specifics for Huawei and Honor devices

There are a couple of things that are different in the Huawei/Honor implementation compared to the other devices in Gadgetbridge.
This page shows the differences.


## Activity/sleep graphs

In the normal activity graphs, the green/blue data differs in height over time. [This shows activity intensity](../../basics/features/activities.md#intensity-of-an-activity).

For the Huawei/Honor devices this value is always the maximum as the watches do not provide a value for this, and no meaningful calculations have been implemented.


## Incorrect sleep detection

The sleep data we get from the band must be interpreted to be shown in Gadgetbridge.
While this mostly works fine, there are some cases in which it is off.
For this case, there are options to ignore the wakeup start status' and wakeup end status' that are received from the band.
They can be found in the `Device specific settings` (the gearwheel on the device card) -> `Force options` -> `Ignore wakeup start status` and `Ignore wakeup end status`.
Either will take effect immediately, which might have effect in the [Activity and Sleep charts](../../basics/features/activities.md#charts).
Be aware that there are also plenty of cases where these settings will not make a difference at all.
There is no risk in changing these settings.


## Retrieving sleep data

The night after the first connection to Gadgetbridge may not be picked up in the sleep synchronization.


## Speed zones

Speed zones are not implemented.
[The chart can be hidden through the chart preferences](../../basics/features/activities.md#manage-charts).


## Small workouts

Workouts that do not contain enough information may not be saved by the watches, and can thus not be synchronized. This will likely only be an issue when testing.


## Workout sync

While [it is normal to only sync a single workout](../../basics/features/sports.md), for Huawei/Honor devices, all workouts from the last fully synchronized one will be synchronized. This may take a moment.


## Sync crash

It is not (always) properly detected that a sync is crashed, meaning that GB stays in the synchronization state. This blocks starting a new synchronization. To retry the synchronization, you have to disconnect and reconnect the device, after which the synchronization can be started again.

*You do __NOT__ need to delete the device from Gadgetbridge for this, disconnecting and reconnecting is enough.*


## Performing multiple operations at the same time

The watches have support to do multiple things at the same time, so synchronizing your workouts while also changing your music should not be an issue.

Note that synchronizing activity and workouts at the same time is disabled, so you will have to do those one after the other.


## Some workout types cannot be started

For example the workout type "Outdoor cycling" (if supported by your device), cannot be chosen on the watch.
We expected this to be due to missing GPS support, but adding GPS support did not change this.


## Widget alarm

The alarm on the [Gadgetbridge widget](../../basics/topics/widgets.md) writes to the "smart alarm" feature, which can go off up to 5 minutes early or late. It also overwrites this alarm, so if you have the smart alarm set, it will be overwritten.


## Smart alarm

There is currently no way to set the interval of the smart alarm.
It is always set to 5 minutes.

Not all devices support the smart alarm, though some devices actually *do* support it while reporting that they don't. So there is an overwrite in the device settings to force enable the smart alarm even when the device reports not supporting it.

Only a single smart alarm is supported, and it is always at the top of the list.

The smart alarm can be marked as unused by long-pressing the alarm in the alarms list. This will stop the alarm from showing up on the band.


## Event alarms

If event alarms are supported by the device, there are exactly 5 event alarms (non-smart).

Each event alarm can be marked as unused by long-pressing the alarm in the alarms list. This will stop the alarm from showing up on the band.


## Heart rate data sharing

While HR logging is supported, sharing the heart rate data is not supported.


## Reparsing the workout data

In the device settings there is a button to "Reparse the workout data". This is because while not all data is known yet, it is still saved. If we implement support for that data at a later time, clicking this button will add the data to all workouts that have been synchronized.

There is no point in clicking this button if no support has been added for new data. It will only waste resources.


## Notification bugs

At the time of writing there are a couple of bugs with receiving notifications:

 - Disable vibrations is not working
 - If multiple Huawei/Honor devices are connected, the notifications do not reliably show up on both devices

The bands also seem to remember at most 10 notifications (may differ per device).


## Call notifications

Call notifications do not stop properly on all devices when the call is started. This can be tested using the debug menu. This seems to only affect devices where the Huawei Health app does not provide any call control support at all.

A workaround is to disable the `Enable rejecting calls` option in the device settings, after which you can reject the calls on the band to get rid of the notification without actually hanging up the call.


## Find my phone

For the "Find my phone" feature to work on newer Android version, the band will need to be connected using Companion Device Pairing.
The only way to currently do that for Huawei/Honor devices is to pair normally first, and then follow the [steps to pair existing devices as companion](../../basics/pairing/companion-device.md#pairing-existing-devices-as-companion).


## Battery synchronization

The battery level is only synchronized on connection, activity sync, and workout sync.


## Restart for SpO2 data

Sometimes the watch fails to get any SpO2 data at all - also after enabling the automatic SpO2 measuring. If this is the case, it seems like restarting the watch fixes the issue.

